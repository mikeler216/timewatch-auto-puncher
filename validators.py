import re


def validate_int(
    _id: str,
):
    if _id.isdigit():
        return True

    return f'Expected number, got "{_id}"'


def validate_time(
    time: str,
):
    if re.match(
        pattern=r'^([0-1]?[0-9]|2[0-3]):[0-5][0-9]$',
        string=time,
    ):
        return True

    return 'Time should match HH:MM pattern.'


def validate_date(
    date: str,
):
    if re.match(
        pattern=r'^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$',
        string=date,
    ):
        return True

    return 'Date should match YYYY-MM-DD pattern.'
