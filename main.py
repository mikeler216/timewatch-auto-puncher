import datetime

import PyInquirer

import daily_punch
import monthly_punch
import validators


def main():
    configuration = PyInquirer.prompt(
        questions=[
            {
                'qmark': '❓',
                'type': 'list',
                'name': 'daily_or_monthly',
                'message': 'Punch daily or monthly?',
                'choices': [
                    'monthly',
                    'daily',
                ],
            },
            {
                'qmark': '⏱️ ',
                'type': 'input',
                'name': 'starttime',
                'default': '09:00',
                'message': 'When do you start your working day?',
                'validate': validators.validate_time,
            },
            {
                'qmark': '⏱️ ',
                'type': 'input',
                'name': 'endtime',
                'default': '18:00',
                'message': 'When do you end your working day?',
                'validate': validators.validate_time,
            },
            {
                'qmark': '🏢 ',
                'type': 'input',
                'name': 'company',
                'default': '4630',
                'message': 'What is your company id?',
                'validate': validators.validate_int,
                'filter': int,
            },
            {
                'qmark': '👥 ',
                'type': 'input',
                'name': 'userid',
                'message': 'What is your user id?',
                'validate': validators.validate_int,
                'filter': int,
            },
            {
                'qmark': '🔒 ',
                'type': 'password',
                'name': 'password',
                'message': 'What is your password?',
            },
        ],
    )

    if configuration['daily_or_monthly'] == 'daily':
        configuration.update(
            PyInquirer.prompt(
                questions=[
                    {
                        'qmark': '🌞 ',
                        'type': 'input',
                        'name': 'punch_date',
                        'default': datetime.date.today().strftime('%Y-%m-%d'),
                        'message': 'When do you want to punch?',
                        'validate': validators.validate_date,
                    },
                ],
            )
        )

        daily_punch.punch_day(
            **configuration,
        )

    else:
        configuration.update(
            PyInquirer.prompt(
                questions=[
                    {
                        'qmark': '📅 ',
                        'type': 'checkbox',
                        'message': 'Select working days.',
                        'name': 'working_days',
                        'choices': [
                            {
                                'name': 'Sunday',
                                'checked': True
                            },
                            {
                                'name': 'Monday',
                                'checked': True
                            },
                            {
                                'name': 'Tuesday',
                                'checked': True
                            },
                            {
                                'name': 'Wednesday',
                                'checked': True
                            },
                            {
                                'name': 'Thursday',
                                'checked': True
                            },
                            {
                                'name': 'Friday',
                                'checked': False
                            },
                            {
                                'name': 'Saturday',
                                'checked': False
                            },
                        ],
                    },
                    {
                        'qmark': '📅 ',
                        'type': 'input',
                        'name': 'month_to_punch',
                        'default': str(datetime.datetime.today().month),
                        'message': 'Select month to punch',
                        'validate': validators.validate_int,
                        'filter': int,
                    },
                ]
            )
        )

        monthly_punch.punch_month(
            **configuration,
        )


if __name__ == '__main__':
    main()
