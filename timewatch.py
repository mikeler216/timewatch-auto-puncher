import datetime
from urllib.parse import urljoin

import requests
from bs4 import BeautifulSoup as BS

import logger
from exceptions import LoginError, PunchError, TimeWatchEror


class TimeWatch:

    def __init__(
        self,
        userid: int,
        company: int,
        password: str,
    ):
        self.logger = logger.get_logger(
            name=self.__class__.__name__,
        )
        self._url = 'https://checkin.timewatch.co.il/'
        self._editpath = 'punch/editwh3.php'
        self._loginpath = 'punch/punch2.php'
        self._login_proof_text = 'דיווחי נוכחות אחרונים'
        self._punch_failed_text = 'אינך מורשה לביצוע פעולה זו!'

        self.userid = userid
        self.company = company
        self.password = password

        self.loggedin = False

        self.session = requests.Session()

    def _post(self, path, data, headers=None):
        full_path = urljoin(self._url, path)
        self.logger.debug(f'Sending request to {full_path}.')
        return self.session.post(full_path, data, headers)

    def _get(self, path, data):
        return self.session.get(urljoin(self._url, path), params=data)

    def login(self) -> None:
        self.logger.info(f'Trying to login as {self.userid}.')
        data = {
            'comp': self.company,
            'name': self.userid,
            'pw': self.password,
        }
        response = self._post(self._loginpath, data, headers=None)
        if self._login_proof_text not in response.text:
            self.logger.error(f'Login as {self.userid} has failed.')
            raise LoginError('Login has failed!')

        self.employeeid = BS(response.text, 'html.parser').find(
            'input', id='ixemplee').get('value')

        self.loggedin = True
        self.logger.info(f'Successfully logged in as {self.userid}.')

    def _time_string_to_tuple(self, time: str) -> tuple:
        time_tuple = tuple(time.strip().split(':'))
        if len(time_tuple) != 2 or len(time) != 5:
            self.logger.critical(f'Time foramt is invalid, got: {time}')
            raise TimeWatchEror(
                'Time format is invalid, Proper format: HH:MM.',
            )
        return time_tuple

    def punch(self, punch_date: str, starttime: str, endtime: str) -> None:
        self.logger.info(f'Trying to punch as {self.userid}.')
        if not self.loggedin:
            self.logger('Please login before trying to punch in.')
            return False
        punch_date_obj = datetime.datetime.strptime(punch_date, '%Y-%m-%d')
        first_day_of_month = punch_date_obj.replace(day=1)

        punch_date_str = punch_date_obj.strftime('%Y-%m-%d')
        first_day_of_month_str = first_day_of_month.strftime('%Y-%m-%d')

        self.session.headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Referer': f'https://checkin.timewatch.co.il/punch/editwh2.php?ie={self.company}&e={self.employeeid}&d={punch_date_str}&jd={first_day_of_month_str}&tl={self.employeeid}',
        }
        start_hour, start_minute = self._time_string_to_tuple(starttime)
        end_hour, end_minute = self._time_string_to_tuple(endtime)

        data = {
            'e': self.employeeid,
            'tl': self.employeeid,
            'c': self.company,
            'd': punch_date_str,
            'jd': first_day_of_month_str,
            'nextdate': '',
            'atypehidden': 0,
            'inclcontracts': 0,
            'job': 0,
            'allowabsence': 3,
            'allowremarks': 1,
            'teken': 0,
            'remark': '',
            'task0': '0',
            'taskdescr0': '',
            'what0': '1',
            'emm0': start_minute,
            'ehh0': start_hour,
            'xmm0': end_minute,
            'xhh0': end_hour,
        }
        response = self._post(self._editpath, data, self.session.headers)
        if self._punch_failed_text in response.text:
            self.logger.info(
                f'Failure punching in on {punch_date_str} from {starttime} to {endtime} as {self.userid}.'
            )
            raise PunchError('Failed to punch-in.')

        self.logger.info(
            f'Punched in on {punch_date_str} from {starttime} to {endtime} as {self.userid}.',
        )
