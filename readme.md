# [TimeWatch](TimeWatch.co.il) automation script

## Decription

---

This script automatically sets working hours for a specific work day or month using timewatch.co.il's web interface. It reads expected work hours and automatically punch in for you.

## How to use?

### Before executing

---

You shuold first install all requierd packages using pip

```bash
pip install -r requirements.txt
```

Now you can execute the script

### Using interacitve cli

---

All you need to do is execute

```python
python3 main.py
```

### Daily demo

![Daily](videos/daily.mp4)

### Monthly demo

![Monthly](videos/monthly.mp4)


