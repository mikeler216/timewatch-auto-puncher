class TimeWatchEror(Exception):
    pass


class LoginError(TimeWatchEror):
    pass


class PunchError(TimeWatchEror):
    pass
